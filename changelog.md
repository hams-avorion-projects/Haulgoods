# Changelog
### 1.0
* Update for Avorion 2.3
* Remove outdated `Custom Craft Orders` library
* Use `Craft Orderz Lib (Shrooblord edit)` instead

### 0.2.5
* Provide compatibility for Avorion 1.3 and higher

### 0.2.4
* Fixed wrong loglevel wich spammed server log

### 0.2.3
* Fixed an issue where the haule bought and then sold a good at the same trading post in endless chain
* Improved mod ai behavior when manually putting goods in hauler cargo

### 0.2.2
* Fixed an issue where alliance ships could not trade correctly sometimes

### 0.2.1
* Updated mod for Avorion 1.0
* Fixed an harmless issue

### 0.2
* Initial public release