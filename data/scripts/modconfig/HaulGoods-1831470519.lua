return {
	{ name ="minTransfer",					value = 1000,				description = "Minimum amount of goods to start a trading route. Selling goods in haulers cargo ignores this value." },
	{ name ="negativeProfitForOwn",			value = true,				description = "If true, hauler will handle trading routes with negative profit, when you or your ally owns target station." },
}