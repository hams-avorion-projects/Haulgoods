package.path = package.path .. ";data/scripts/lib/?.lua"


OrderChain.registerModdedCraftOrder(OrderType.Haulgoods, {
    title = "HaulGoods",
    callback = "onUserHaulGoodsOrder"
})

OrderChain.registerModdedOrderChain(OrderType.Haulgoods, {
    isFinishedFunction = "HaulGoodsOrderFinished",
    canEnchainAfter = true,
    onActivateFunction = "activateHaulGoods",
    canEnchainAfterCheck = "canEnchainAfterHaulGoods",
});

function OrderChain.onUserHaulGoodsOrder()
    if onClient() then
        invokeServerFunction("onUserHaulGoodsOrder")
        ScriptUI():stopInteraction()
        return
    end

    OrderChain.clearAllOrders()
    OrderChain.addHaulGoodsOrder()
    OrderChain.runOrders()
end
callable(OrderChain, "onUserHaulGoodsOrder")

function OrderChain.addHaulGoodsOrder()
    local player
    if callingPlayer then
        local owner, _
        owner, _, player = checkEntityInteractionPermissions(Entity(), AlliancePrivilege.ManageShips)
        if not owner then return end

        if not OrderChain.canReceivePlayerOrder() then return end
    end

    local targetId
    if player and player.craft then
        targetId = player.craft.id
    end

    local order = {action = OrderType.Haulgoods, targetId = targetId}

    if OrderChain.canEnchain(order) then
        OrderChain.enchain(order)
    end
end
callable(OrderChain, "addHaulGoodsOrder")

function OrderChain.activateHaulGoods(order)
    print(order.targetId.string)

    OrderChain.updateAutoPilotStatus()

    Entity():addScriptOnce("ai/haulgoods.lua", order.targetId, true)
    return true
end
callable(OrderChain, "activateHaulGoods")

function OrderChain.HaulGoodsOrderFinished(order)
    return not Entity():hasScript("data/scripts/entity/ai/haulgoods.lua")
end
callable(OrderChain, "HaulGoodsOrderFinished")

function OrderChain.canEnchainAfterHaulGoods(order)
    return false
end
