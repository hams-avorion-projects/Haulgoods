
package.path = package.path .. ";data/scripts/lib/?.lua"
package.path = package.path .. ";data/scripts/?.lua"
package.path = package.path .. ";data/scripts/entity/?.lua"
package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"


-- TODO: how about your own consumer stations? Upd: Treat like normal stations(?)
-- TODO: move EntityHelper and extended TradingManager to a new mod: HamLib

include ("utility")
include ("extutils")
include ("goods")
local DockAI = include ("entity/ai/dock")
local ShipAI = ShipAI()

local TradingUtility = include("tradingutility")
local EH = {} -- Entity helper


local ConfigLib = include("ConfigLib")
local HaulgoodsConfigLib = ConfigLib("1831470519") -- Mod ID is configured in modinfo.lua

local self


-- Don't remove or alter the following comment, it tells the game the namespace this script lives in. If you remove it, the script will break.
-- namespace Haulgoods
Haulgoods = {}


if onServer() then -- OnServer Start

local workingUpdateInterval = 0.05
local idleUpdateInterval = 10

local updateInterval = workingUpdateInterval

local States =
{
    Buy = 0,
    Sell = 1,
    Wait = 2,
}


-- Default empty route
function emptyRoute()
	return {
		Vendor = nil,			-- Entity
		VendorScript = "",		-- Vendors trading script
		Target = nil,			-- Entity
		TargetScript = "",		-- Targets trading script
		Goods = {},				-- { {name = goodname, amount = 1, priceVendor = 0, priceTarget = 0}, }
		State = States.Wait,	-- State
		Priority = 0			-- Only used when finding a new route yet
	}
end

local Route = emptyRoute()


function Haulgoods.getUpdateInterval()
    return updateInterval
end


-- DockAI functions
function doTransaction(ship, station)
	HaulgoodsConfigLib.log(3, "Transaction: doTransaction()")
	local script
	local invokedFnc
	
	if Route.State == States.Buy then
		-- when the ship buys, the station sells to the ship
		invokedFnc = "sellToShip"
		script = Route.VendorScript
	elseif Route.State == States.Sell then
		-- when the ship sells, the station buys from the ship
		-- TODO: refresh goods amount - station cargo might have changed
		invokedFnc = "buyFromShip"
		script = Route.TargetScript
	end
	
	HaulgoodsConfigLib.log(3, "Transaction:", invokedFnc)
	
	if invokedFnc then
		for _,good in pairs(Route.Goods) do
			local ok, ret = station:invokeFunction(script, invokedFnc, ship.index, good.name, good.amount, true)
			if ok == 0 then
				HaulgoodsConfigLib.log(3, "Transaction: traded", good.amount, good.name)
			else
				HaulgoodsConfigLib.log(1, "Transaction: Error. Could not execute transaction:", ok)
			end
		end
	end
end

function finished()
	HaulgoodsConfigLib.log(3, "Transaction: finished()")
	if Route.State == States.Buy then
		DockAI.reset()
		Route.State = States.Sell
	else
		Haulgoods.resetRoute()
	end
end

function Haulgoods.initialize()
	self = Entity()
	
	if not _restoring then
		Haulgoods.updateServer(0, true)
	end
	-- TODO: Remove if entity is a station
end

function Haulgoods.updateServer(timeStep, initialize)
    if not (self:hasScript("tradingoverview.lua") or self:hasScript("internal/dlc/rift/systems/hypertradingsystem.lua")) then
        ShipAI:setStatus("You have to install a Trading System in the ship for haul goods to work."%_T, {})
		ShipAI:setPassive()
		Faction():sendChatMessage("", ChatMessageType.Error, "You have to install a Trading System in the ship for haul goods to work."%_T)
		terminate()
        return
    end
	if self.isStation then
		Faction():sendChatMessage("", ChatMessageType.Error, "Stations can not do this."%_T)
		terminate()
	end
	
    if Haulgoods.validRoute() then
		local station
		if Route.State == States.Buy then
			station = Route.Vendor
		elseif Route.State == States.Sell then
			station = Route.Target
		end
		--							 (timeStep, station, dockWaitingTime, doTransaction, finished, skipUndocking)
		DockAI.updateDockingUndocking(timeStep, station, 5, doTransaction, finished)
    else
		if Haulgoods.findRoute() then
			updateInterval = workingUpdateInterval
		else
			if Route.State ~= States.Wait then -- Invalid route selected - reset it
				Haulgoods.resetRoute()
				ShipAI:setPassive()
				HaulgoodsConfigLib.log(3, "No routes found - set waiting")
			end
			if initialize then
				Faction():sendChatMessage("", ChatMessageType.Error, "No valid trading route found - waiting."%_T)
				ShipAI:setPassive()
				HaulgoodsConfigLib.log(3, "No routes found - set waiting")
			end
			updateInterval = idleUpdateInterval
		end
    end
end


-- Let other ships/scripts in same sector interact with haulgoods
function Haulgoods.getRoute()
	return Route
end

-- Other mods may modify the route. F.e. other stations could actively request goods or so
function Haulgoods.setRoute(incroute)
	if Haulgoods.validRoute(incroute) then
		Route = incroute
	else
		HaulgoodsConfigLib.log(3, incroute, "setRoute() - route is not valid")
	end
end

function Haulgoods.getHaulers()
	return {Sector():getEntitiesByScript("haulgoods.lua")}
end


function Haulgoods.validRoute(incroute)
	incroute = incroute or Route
	-- Does stations still exist?
	if ((not valid(incroute.Vendor) or incroute.VendorScript == "") and incroute.State == States.Buy)
	or not valid(incroute.Target)
	or incroute.TargetScript == ""
	then
		HaulgoodsConfigLib.log(4, "No valid vendor/target - route is invalid")
		return false
	end
	
	-- Are goods set?
	if tablelength(incroute.Goods) == 0 or not incroute.Goods[1]["amount"] or incroute.Goods[1]["amount"] < 1 then
		HaulgoodsConfigLib.log(4, "No goods set or amount too low - route is invalid")
		return false
	end
	
	-- Is the hauler currently working?
	if incroute.State ~= States.Buy and incroute.State ~= States.Sell then
		HaulgoodsConfigLib.log(4, "No active state - route is invalid:", incroute.State)
		return false
	end
	
	return true
end


function Haulgoods.checkRoute(incroute, haulers)
	incroute = incroute or Route
	haulers = haulers or Haulgoods.getHaulers()
	
	if not Haulgoods.validRoute(incroute) then
		return false
	end
	
	for _,hauler in pairs(haulers) do
		if self.index ~= hauler.index then
			local ok, otherroute = hauler:invokeFunction("haulgoods.lua", "getRoute")
			
			if ok == 0 then
				if Haulgoods.validRoute(otherroute) and (incroute.Vendor.index == otherroute.Vendor.index or incroute.Target.index == otherroute.Target.index) then
					HaulgoodsConfigLib.log(4, "Check route: route matched")
					return false
				end
			else
				HaulgoodsConfigLib.log(1, "Could not load route from other hauler:", ok)
			end
		end
	end
	
	return true -- No conflicting route found - route can be used
end


function Haulgoods.findRoute()
	HaulgoodsConfigLib.log(3, "findRoute()")
	local ret = false
	
	local timer = HighResolutionTimer()
	local subtimer = HighResolutionTimer()
	timer:start()
	
	local routes = {}
	
	
	subtimer:start()
	local goods = Haulgoods.loadGoodsList()
	subtimer:stop()
	HaulgoodsConfigLib.log(2, "PerformanceDebugging - loadGoodsList():", subtimer.microseconds / 1000 .. "ms")
	subtimer:reset()
	
	
	local weightBuy = 0.5
	local weightSell = 1
	local ownStationsPriority = 60
	local minTransfer = HaulgoodsConfigLib.get("minTransfer")
	
	
	local selfCargoBay = CargoBay():getCargos()
	local selfCargos = {}
	
	for tradingGood, amount in pairs(selfCargoBay) do -- Loop own cargos
		selfCargos[tradingGood.name] = amount
	end
	
	subtimer:start()
	
	for good,tradinglist in pairs(goods) do -- Loop goodslist
		HaulgoodsConfigLib.log(4, "find route for good", good)
		
		if (tablelength(tradinglist[States.Buy]) > 0 or selfCargos[good]) and tablelength(tradinglist[States.Sell]) > 0 then
			local tmpRoute = emptyRoute()
			tmpRoute.Goods = {{name = good, amount = 0, priceVendor = 0, priceTarget = 0}} -- TODO: implement other goods
			tmpRoute.Priority = 0
			
			local canBuy = 0
			
			local buyerPriority = 0
			
			if selfCargos[good] then -- Self cargo bay - use this first
				tmpRoute.Vendor = self		-- Entity() is the vendor
				tmpRoute.VendorScript = "self"
				buyerPriority = selfCargos[good] + 1000000 -- Always has higher priority then trading routes
				
				canBuy = selfCargos[good]
			else
				-- Vendors
				local highestStock = 0 -- in %
				
				for stationIndex,tradeable in pairs(tradinglist[States.Buy]) do -- Loop vendors
					local station = Entity(stationIndex)
					local ok, tradingPossible, buyingAllowed, sellingAllowed = station:invokeFunction(tradeable.script, "tradingPossible", self.index) -- TODO: check if entity.buyFromOthers or isOwned
					
					if ok == 0 and buyingAllowed then
						local isOwnPriority = 0
						if EH.isOwned(station) then isOwnPriority = ownStationsPriority end -- Higher stock is more important
						
						
						local curStock = (tradeable.stock / tradeable.maxStock * 100) + isOwnPriority -- in %
						
						if curStock > highestStock and minTransfer <= tradeable.stock then
							tmpRoute.Vendor = station
							tmpRoute.VendorScript = tradeable.script
							buyerPriority = curStock * weightBuy
							tmpRoute.Goods[1]["priceVendor"] = tradeable.price
							
							canBuy = tradeable.stock
							highestStock = curStock
						end
					end
				end
			end
			
			local canSell = 0
			if tmpRoute.Vendor then
				local lowestStock = 99999999 -- in % - be sure that its high enaugh
				
				for stationIndex,tradeable in pairs(tradinglist[States.Sell]) do -- Loop targets
					local station = Entity(stationIndex)
					local ok, tradingPossible, buyingAllowed, sellingAllowed = station:invokeFunction(tradeable.script, "tradingPossible", self.index) -- TODO: check if entity.sellToOthers or isOwned
					
					if ok == 0 and sellingAllowed then
						local isOwnPriority = 0
						local isOwned = EH.isOwned(station)
						
						if isOwned then isOwnPriority = ownStationsPriority end -- Higher stock is more important
						
							
						local curStock = (tradeable.stock / tradeable.maxStock * 100) - isOwnPriority -- in %
						
						-- Damn ugly if statement... cant it be smarter? TODO
						if curStock < lowestStock
						and 1 <= tradeable.maxStock - tradeable.stock
						and (tradeable.script ~= "/consumer.lua" or not isOwned) -- never sell to own consumer
						and (tradeable.price >= tmpRoute.Goods[1]["priceVendor"] or (HaulgoodsConfigLib.get("negativeProfitForOwn") and isOwned))
						and (minTransfer <= tradeable.maxStock - tradeable.stock)
						and tmpRoute.Vendor.index ~= station.index
						then
							tmpRoute.Target = Entity(stationIndex) -- Todo: use 'station' var instead?
							tmpRoute.TargetScript = tradeable.script
							tmpRoute.Priority = buyerPriority + ((100 - curStock) * weightSell)
							tmpRoute.Goods[1]["priceTarget"] = tradeable.price
							
							canSell = tradeable.maxStock - tradeable.stock
							lowestStock = curStock -- Less is more improtant
						end
					end
				end
			end
			
			if tmpRoute.VendorScript == "self" then
				tmpRoute.State = States.Sell
			else
				tmpRoute.State = States.Buy
			end
			--tmpRoute.Goods = {{name = good, amount = math.min(canBuy, canSell)}}
			tmpRoute.Goods[1]["amount"] = math.min(canBuy, canSell)
			
			if Haulgoods.validRoute(tmpRoute) then
				table.insert(routes, tmpRoute)
			end
		end
	end
	
	subtimer:stop()
	HaulgoodsConfigLib.log(2, "PerformanceDebugging - process goods list:", subtimer.microseconds / 1000 .. "ms")
	subtimer:reset()
	
	local haulers = Haulgoods.getHaulers()
	
	
	for _,curroute in spairs(routes, function(t,a,b) return routes[b].Priority < routes[a].Priority end) do
		if Haulgoods.checkRoute(curroute) then
			Route = curroute
			HaulgoodsConfigLib.log(2, curroute, "Set new trading route for ship", self.name .. ":")
			ret = true
			goto continue
		end
	end
	
	::continue::
	
	timer:stop()
	HaulgoodsConfigLib.log(2, "PerformanceDebugging - findRoute():", timer.microseconds / 1000 .. "ms")
	timer:reset()
	
	return ret
end


function Haulgoods.loadGoodsList()
	-- find stations that sell the goods
	local goodlist = {}
	
	local subtimer = HighResolutionTimer()
	subtimer:start()
	
	local OverviewSellables, OverviewBuyables = TradingUtility.detectBuyableAndSellableGoods()
	
	subtimer:stop()
	HaulgoodsConfigLib.log(2, "PerformanceDebugging - get trading data:", subtimer.microseconds / 1000 .. "ms")
	subtimer:reset()
	
	
	if type(OverviewSellables) ~= "table" or not next(OverviewSellables) or type(OverviewBuyables) ~= "table" or not next(OverviewBuyables) then
		HaulgoodsConfigLib.log(3, "Could not find any buyer and/or seller in current sector")
        return goodlist
    end
	
	-- Do stuff in an inline function for not having the code twice while having access to goodlist
	local addToGoodList = function(tradeable, action)
		HaulgoodsConfigLib.log(4, "Create goodlist:", tradeable.good.name)
		local good = tradeable.good.name
		
		if not goodlist[good] then
			goodlist[good] = {}
			goodlist[good][States.Buy] = {}
			goodlist[good][States.Sell] = {}
		end
		
		--table.insert(goodlist[good][action], tradeable)
		goodlist[good][action][tradeable.stationIndex.string] = tradeable
	end
	
	subtimer:start()
	for _,sellable in pairs(OverviewSellables) do
		addToGoodList(sellable, States.Sell)
	end
	
	for _,buyable in pairs(OverviewBuyables) do
		addToGoodList(buyable, States.Buy)
	end
	subtimer:stop()
	HaulgoodsConfigLib.log(2, "PerformanceDebugging - process trading data:", subtimer.microseconds / 1000 .. "ms")
	subtimer:reset()
	
	return goodlist
end


function Haulgoods.resetRoute()
	DockAI.reset()
	ShipAI:setPassive()
	Route = emptyRoute()
end


-- Entity helper
EH.isOwned = function(entity)
	if self.factionIndex == entity.factionIndex
	or (self.playerOwned and entity.allianceOwned and Player(self.factionIndex).allianceIndex == entity.factionIndex)
	then
		return true
	end
	
	return false
end



end -- OnServer End
