
local HaulgoodsBuyFromShipParent = TradingManager.buyFromShip
function TradingManager:buyFromShip(shipIndex, goodName, amount, noDockCheck)
	local origBuyFromOthers = self.buyFromOthers
	
	local canTrade, canTake, canAdd = self:tradingPossible(shipIndex)
	
	if not origBuyFromOthers and canAdd then -- When the ship adds, the station buys
		self.buyFromOthers = true
	end
    
	local ret = HaulgoodsBuyFromShipParent(self, shipIndex, goodName, amount, noDockCheck)
	
	if not origBuyFromOthers and canAdd then
		self.buyFromOthers = false
	end
	
	return ret
end

local HaulgoodsSellToShipParent = TradingManager.sellToShip
function TradingManager:sellToShip(shipIndex, goodName, amount, noDockCheck)
	local origSellToOthers = self.sellToOthers
	
	local canTrade, canTake, canAdd = self:tradingPossible(shipIndex)
	
	if not origSellToOthers and canTake then -- When the ship takes, the station sells
		self.sellToOthers = true
	end
    
	local ret = HaulgoodsSellToShipParent(self, shipIndex, goodName, amount, noDockCheck)
	
	if not origSellToOthers and canTake then
		self.sellToOthers = false
	end
	
	return ret
end


-- returns multiple bool values: tradingPossible, canBuy, canSell
function TradingManager:tradingPossible(shipIndex)
	local stationFaction = Faction()
	local ship = Entity(shipIndex)
	local player = Player(ship.factionIndex)
	if not ship or ship.aiOwned then return end
	
	if ship.factionIndex == stationFaction.index then
		return true, true, true
	end
	
	-- Throws issues since Avorion 1.3
	-- Todo: replace??
	--if player and not CheckFactionInteraction(ship.factionIndex, self.relationsThreshold or 0) then
	--	return false, false, false
	--end
	
	local canTake = self:getSellsToOthers()
	local canAdd = self:getBuysFromOthers()
	
	
	local station = Entity()
	
	
	if player and station.allianceOwned and not (canTake and canAdd) and player.allianceIndex == station.factionIndex then
		if not canTake then
			canTake = player.alliance:hasPrivilege(player.index, AlliancePrivilege.TakeItems)
		end
		
		if not canAdd then
			canAdd = player.alliance:hasPrivilege(player.index, AlliancePrivilege.AddItems)
		end
	end
	
	
	return canTake or canAdd, canTake, canAdd
end



local HaulgoodsCreateNamespaceParent = PublicNamespace.CreateNamespace
function PublicNamespace.CreateNamespace()
	local result = HaulgoodsCreateNamespaceParent()
	
	result.getRelationsThreshold = function() return result.trader.relationsThreshold end
	result.tradingPossible = function(...) return result.trader:tradingPossible(...) end
	
	return result
end





