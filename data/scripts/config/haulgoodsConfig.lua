
package.path = package.path .. ";data/scripts/config/?.lua"

include("sMConf")

local HaulGoodsConf = {
    modID = "HaulGoods",
    develop = false,
    dbgLevel = 0,       --0 = off; 1 = info; 2 = verbose; 3 = extremely verbose; 4 = I WANT TO KNOW EVERYTHING

    sayHiTimes = 0,
}

table.insert(sMConf, HaulGoodsConf)

return HaulGoodsConf
