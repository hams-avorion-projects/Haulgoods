# Haul Goods

This mod is a new ship command you can use when interacting with a ship by pressing 'F'. The intention of this command is to handle trading routes to automate your production chains.

It will pick the most urgent trading route (watch `Priorities` for more information) and deliver the goods from the producer/seller to a station that buys this good.

You can also put any goods into the ships cargo, and it will sell it to the station, that need it most.

* It will buy goods from a station you (or your alli) does not own, when an own station needs this good
* It will sell goods to stations you (or your alli) does not own when the related trading route profit is not negative

You can modify the AI via configs, watch `Configuraion` section below.


### Priorities
The AI choses its next (or first) trading route by the availble/required goods at seller/buyer (percentaged). Means if a station needs energy cells for production and got 2000 in cargo with a max total of 25000, this station will be favored compared to a station that has 1000 energy cells with a max stock of 10000 (same for goods the station sells).

When the ship has goods in its cargo that you manually put into, it will try to sell them before it will look for a new trading route (a currently running trade will be finished before).

Because the intention of this mod is to supply your own stations with goods, it will **not** chose the route with the best profit (wich is obsolete if you own these stations).

Stations that are not owned by you or your alli are always less important compared to stations you own.

### Configuration (optional)
The mod `ConfigLib` provides the configuration for this mod. To modify the AI you may take a look at the [wiki pages](https://gitlab.com/hams-avorion-projects/configlib/wikis/home) pages for more informations.


### Credits
Concept of this mod initially released by [Splutty](https://www.avorion.net/forum/index.php/topic,2896.0.html).
